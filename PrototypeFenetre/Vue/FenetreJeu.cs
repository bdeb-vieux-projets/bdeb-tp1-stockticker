﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StockTicker;
namespace StockTicker
{
    public partial class FenetreJeu : Form 
    {
        private Jeu _instanceJeu;
        private int _joueurActif;
        public FenetreJeu(int nbreTours, int nbreJoueurs)
        {            
            _instanceJeu = new Jeu(nbreTours,nbreJoueurs);            
            InitializeComponent(_instanceJeu.NbreJoueurs, _instanceJeu.ListeJoueurs, _instanceJeu.ValeursActions);      
        }
       
        private void btnRoulerDes_Click(object sender, EventArgs e)
        {
            if (_instanceJeu.ToursFinis < _instanceJeu.NbreTours)
            {
                txtHistorique.AppendText(this._instanceJeu.VerifieValeursAction());
                this._instanceJeu.JouerAi();
                txtHistorique.AppendText(_instanceJeu.ActionsAI);
                ReinitialiserSpinners();
                String resultatDes = this._instanceJeu.RoulerLesDes();
                lblResultsDes.Text = resultatDes;
                txtHistorique.AppendText(resultatDes);
                UpdateTableActions(this._instanceJeu.ValeursActions);
                UpdateTours(_instanceJeu.ToursFinis, _instanceJeu.NbreTours);
            }
            else
            {
                btnRoulerDes.Enabled = false;
                btnAcheter.Enabled = false;
                btnVendre.Enabled = false;             
                MessageBox.Show("Fin de la partie.\n",
                    "Stock Ticker", MessageBoxButtons.OK, MessageBoxIcon.Information);            
            }
            UpdateTableScores(this._instanceJeu.ListeJoueurs);
        }

        private void btnAcheter_Click(object sender, EventArgs e)
        {
           txtHistorique.AppendText(this._instanceJeu.AcheterAction(_instanceJeu.ListeJoueurs[_joueurActif].IdJoueur, _instanceJeu.ListeJoueurs[_joueurActif].NoJoueur));
           this.UpdateTableScores(_instanceJeu.ListeJoueurs);
           ReinitialiserSpinners();
        }        

        private void numOr_ValueChanged(object sender, EventArgs e)
        {
            Console.WriteLine("Or :" + numOr.Value);
           _instanceJeu.ActionsSpinners[(int)IDActions.OR] =(int)numOr.Value;
        }

        private void numArgent_ValueChanged(object sender, EventArgs e)
        {
            Console.WriteLine("argent :" + numArgent.Value);
            _instanceJeu.ActionsSpinners[(int)IDActions.ARGENT] = (int)numArgent.Value;
        }

        private void numPetrole_ValueChanged(object sender, EventArgs e)
        {
            Console.WriteLine("petrole :" + numPetrole.Value);
            _instanceJeu.ActionsSpinners[(int)IDActions.PETROLE] = (int)numPetrole.Value;
        }

        private void numBons_ValueChanged(object sender, EventArgs e)
        {
            Console.WriteLine("bons :" + numBons.Value);
            _instanceJeu.ActionsSpinners[(int)IDActions.BONS] = (int)numBons.Value;
        }

        private void numIndustrielles_ValueChanged(object sender, EventArgs e)
        {
            Console.WriteLine("indus :" + numIndustrielles.Value);
            _instanceJeu.ActionsSpinners[(int)IDActions.INDUSTRIEL] = (int)numIndustrielles.Value;
        }

        private void numGrains_ValueChanged(object sender, EventArgs e)
        {
            Console.WriteLine("grains :" + numGrains.Value);
            _instanceJeu.ActionsSpinners[(int)IDActions.GRAIN] = (int)numGrains.Value;
        }

        private void FenetreJeu_Validated(object sender, EventArgs e)
        {
            //MettreAJour();
        }

        private void btnVendre_Click(object sender, EventArgs e)
        {
           txtHistorique.AppendText(_instanceJeu.VendreAction(_instanceJeu.ListeJoueurs[_joueurActif].IdJoueur, _instanceJeu.ListeJoueurs[_joueurActif].NoJoueur));
           this.UpdateTableScores(_instanceJeu.ListeJoueurs);
           ReinitialiserSpinners();
        }

        private void quitterToolStripMenuItem_Click(object sender, EventArgs e)
        {
           // Application.Restart();
            this.Close();
        }

        private void nouvellePartieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }
        private void paramsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //NYI
        }
        /// <summary>
        /// Fonction qui remet à 0 la valeur des numericUpDown.
        /// </summary>
        private void ReinitialiserSpinners()
        {
            numOr.Value = 0;
            numArgent.Value = 0;
            numPetrole.Value = 0;
            numBons.Value = 0;
            numIndustrielles.Value = 0;
            numGrains.Value = 0;
            this._instanceJeu.ReinitialiserActionSpinners();
        }
       
    }
}
