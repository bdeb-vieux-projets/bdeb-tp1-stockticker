﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace StockTicker.Vue
{
    public partial class FenetreParamsDebutJeu : Form
    {
        private int _nbreTours = 30;
        private int _nbreJoueurs = 4;
        public FenetreParamsDebutJeu()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.Hide();
            FenetreJeu jeu = new FenetreJeu(_nbreTours,_nbreJoueurs);
            jeu.ShowDialog();
            this.Close();
        }

        private void optDixTours_CheckedChanged(object sender, EventArgs e)
        {
            _nbreTours = 10;
        }

        private void optTrenteTours_CheckedChanged(object sender, EventArgs e)
        {
            _nbreTours = 30;
        }

        private void optSoixanteTours_CheckedChanged(object sender, EventArgs e)
        {
            _nbreTours = 60;
        }

        private void optDeuxJoueurs_CheckedChanged(object sender, EventArgs e)
        {
            _nbreJoueurs = 2;
        }

        private void optTroisJoueurs_CheckedChanged(object sender, EventArgs e)
        {
            _nbreJoueurs = 3;
        }

        private void optQuatreJoueurs_CheckedChanged(object sender, EventArgs e)
        {
            _nbreJoueurs = 4;
        }
    }
}
