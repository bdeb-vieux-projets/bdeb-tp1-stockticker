﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StockTicker;
using System.Windows.Forms;
using StockAI;

namespace StockTicker
{
    public class Jeu
    {
       // private Joueur _joueurActif;
        private int _nbreJoueurs;
        public int NbreJoueurs
        {
            get { return _nbreJoueurs; }
            set { _nbreJoueurs = value; }
        }

        public readonly String[] NOM_ACTIONS = { "OR", "ARGENT", "PETROLE", "BONS", "INDUSTRIEL", "GRAIN" };
        private IDActions[] _IDActionsAI = { IDActions.GRAIN, IDActions.INDUSTRIEL, IDActions.BONS, IDActions.PETROLE, IDActions.ARGENT, IDActions.OR};
        public Jeu(int nbreTours, int nbreJoueurs)
        {
            this._nbreJoueurs = nbreJoueurs;
            this._nbreTours = nbreTours;
            InitialiserPartie();
        }
                
        /// <summary>
        /// Fonction qui initialise une partie.
        /// </summary>
        private void InitialiserPartie()
        {
            this.ActionsSpinners = new int[6];
            this.ValeursActions = new Action[6];
            _des = new Des(); 
            InitialiserJoueurs();
            InitialiserActionSpinner();
            InitialiserValeursActionsBourse();
            _joueurActif = 0;
        }
        /// <summary>
        /// Fonction qui initialise les joueurs.
        /// </summary>
        private void InitialiserJoueurs()
        {
            Joueur unJoueur;
            this.ListeJoueurs = new List<Joueur>();           

            for (int i = 1; i <= this._nbreJoueurs; ++i)
            {
                
                if (i == 1) //Cas où le joueur est humain.
                {
                    unJoueur = new Joueur(IDJoueur.HUMAIN, "Humain", i, 5000);
                    ListeJoueurs.Add(unJoueur);
                }
                else //Cas où le joueur est un AI.
                {
                    unJoueur = new Joueur(IDJoueur.AI, ("Ordinateur " + Convert.ToString(i-1)), i, 5000);
                    ListeJoueurs.Add(unJoueur);
                }
                unJoueur = null;
            }
        }
        /// <summary>
        /// Fonction qui initialise la valeur des 
        /// </summary>
        private void InitialiserActionSpinner()
        {
            this.ActionsSpinners = new int[6];
            for (int i = 0; i < this.ActionsSpinners.Length; ++i)
            {
                this.ActionsSpinners[i] = 0;
            }
        }
        /// <summary>
        /// Fonction qui initialise la valeur des actions à leurs valeurs initiales.
        /// </summary>
        private void InitialiserValeursActionsBourse()
        {
            ValeursActions = new Action[6];
            ValeursActions[0] = new Action(IDActions.GRAIN, 100, 1);
            ValeursActions[1] = new Action(IDActions.INDUSTRIEL, 100, 1);
            ValeursActions[2] = new Action(IDActions.BONS, 100, 1);
            ValeursActions[3] = new Action(IDActions.PETROLE, 100, 1);
            ValeursActions[4] = new Action(IDActions.ARGENT, 100, 1);
            ValeursActions[5] = new Action(IDActions.OR, 100, 1);

        }
        /// <summary>
        /// Fonction qui vend une action.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="numeroJoueur"></param>
        /// <returns></returns>
        public String VendreAction(IDJoueur id, int numeroJoueur) 
        {
            String msgAchat = "";
            int qteAction = 0;
            foreach (Joueur joueur in ListeJoueurs)
            {
                if (joueur.IdJoueur == id && joueur.NoJoueur == numeroJoueur)
                {
                    for (int i = 0; i < ActionsSpinners.Length; ++i)
                    {
                        qteAction = ActionsSpinners[i];
                        if (qteAction > 0)
                        {
                            IDActions action = (IDActions)i;
                            if (joueur.Actions[i].QteAction >= qteAction)
                            {
                                joueur.Cash += qteAction * ValeursActions[i].ValeurAction/100;
                                SoustraireAction(joueur, action, qteAction);
                                Console.WriteLine("Cash : " + joueur.Cash);
                                msgAchat += "\n" + joueur.NomJoueur + " vend " + Convert.ToString(qteAction) + " " + NOM_ACTIONS[i].ToString() + System.Environment.NewLine;
                            }
                            else if (joueur.IdJoueur != IDJoueur.AI)
                            {
                                MessageBox.Show("Nombre d'actions invalides! Veuillez recommencer.", "Stock Ticker", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                }
            }
            return msgAchat;
        }
        /// <summary>
        /// Fonction qui achète une action.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="numeroJoueur"></param>
        public String AcheterAction(IDJoueur id, int numeroJoueur)
        {
            String msgAchat = "";
            int qteAction = 0;
            foreach (Joueur joueur in ListeJoueurs)
            {
                if (joueur.IdJoueur == id && joueur.NoJoueur == numeroJoueur)
                {
                    for (int i = 0; i < ActionsSpinners.Length; ++i)
                    {
                        qteAction = ActionsSpinners[i];
                        if (qteAction > 0)
                        {
                            if (joueur.Cash >= qteAction * ValeursActions[i].ValeurAction/100)
                            {
                                IDActions action = (IDActions)i;
                                joueur.Cash -= qteAction * ValeursActions[i].ValeurAction/100;
                                AjouterActions(joueur, action, qteAction);
                                Console.WriteLine("Cash : " + joueur.Cash);
                                msgAchat += "\n" + joueur.NomJoueur + " achete " + Convert.ToString(qteAction) + " " + NOM_ACTIONS[i].ToString() + System.Environment.NewLine;
                            }
                            else if (joueur.IdJoueur != IDJoueur.AI)
                            {
                                MessageBox.Show("Vous n'avez pas assez d'argent!", "Stock Ticker", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }                       
                    }
                }
            }
            return msgAchat;
        }        
        /// <summary>
        /// Fonction qui soustrait un nombre donné d'une action donné vendu par un joueur.
        /// </summary>
        /// <param name="joueur"></param>
        /// <param name="id"></param>
        /// <param name="qteAction"></param>
        private void SoustraireAction(Joueur joueur, IDActions id, int qteAction)
        {
            foreach(Action act in joueur.Actions)
            {
                if (act.IdAction == id)
                {
                    act.QteAction -= qteAction;
                }
            }
        }
        /// <summary>
        /// Fonction qui ajoute un nombre donné d'une action donné vendu par un joueur.
        /// </summary>
        /// <param name="joueur"></param>
        /// <param name="id"></param>
        /// <param name="qteAction"></param>
        private void AjouterActions(Joueur joueur, IDActions id, int qteAction)
        {            
            foreach (Action act in joueur.Actions)
            {
                if (act.IdAction == id)
                {
                    act.QteAction += qteAction;
                }
            }                
        }
        /// <summary>
        /// Fonction qui roule les dés.
        /// </summary>
        /// <returns></returns>
        public String RoulerLesDes()
        {
            String resultat = "";
            if (this._toursFinis < this._nbreTours)
            {
                ++this._toursFinis;
                _des.RoulerDes();
                resultat += _des.DeAction;
                resultat += " ";
                resultat += _des.DeFluctuation;
                resultat += " ";
                resultat += _des.DeValeur;
                resultat += "\n";
               
                UpdateStockValues();
                for (int i = 0; i < ValeursActions.Length; ++i)
                {
                    Console.WriteLine(ValeursActions[i].ValeurAction);
                }
            }
            
            return resultat;
        }

       /// <summary>
       /// Fonction qui met à jour la valeur des actions selon les résultats des dés et qui prend
       /// en charge les dividendes.
       /// </summary>
        public void UpdateStockValues()
        {
            int action = (int)_des.DeAction;
            switch (_des.DeFluctuation)
            {
                case "monte de":
                    ValeursActions[action].DerniereFluctuation = "+ " + Convert.ToString(_des.DeValeur);
                    this._valeursActions[action].ValeurAction += _des.DeValeur ;
                    break;
                case "descends de":
                    ValeursActions[action].DerniereFluctuation = "- " + Convert.ToString(_des.DeValeur);
                    this._valeursActions[action].ValeurAction -= _des.DeValeur ;
                    break;
                case "paye":
                    if (this._valeursActions[action].ValeurAction >= 100)
                    {
                        PayerLesJoueurs(_des.DeValeur, action);
                    }
                    break;
            }
        }
        /// <summary>
        /// Fonction qui paye les joueurs dans le cas d'un dividende.
        /// </summary>
        /// <param name="valeurDes"></param>
        /// <param name="valeurAction"></param>
        /// <param name="indiceAction"></param>
        private void PayerLesJoueurs(int valeurDes, int indiceAction)
        {
            foreach (Joueur joueur in this._listeJoueurs)
            {
                if (joueur.Actions[indiceAction].QteAction != 0)
                {
                    joueur.Cash += Convert.ToInt32(joueur.Actions[indiceAction].QteAction * Convert.ToDouble(_des.DeValeur) / 100); 
                }
            }            
        }
        /// <summary>
        /// Fonction qui se charge de faire jouer les AI.
        /// </summary>
        public void JouerAi()
        {
            int[] deltaQuantitesActions = new int[6];
            int[] quantitesActions = new int[6];
            int[] positionActions = new int[] { this.ValeursActions[5].ValeurAction, this._valeursActions[4].ValeurAction,  
                this._valeursActions[3].ValeurAction,   this._valeursActions[2].ValeurAction,  
                this._valeursActions[1].ValeurAction,   this._valeursActions[0].ValeurAction };

            foreach (Joueur joueur in this._listeJoueurs)
            {
                if (joueur.IdJoueur == IDJoueur.AI)
                {
                    quantitesActions = CreateTableAI(joueur);

                    if (StockTickerAI.ActionAI(joueur.NoJoueur, positionActions,
                        quantitesActions, joueur.Cash, out deltaQuantitesActions)) //si le AI a fait un achat
                    {
                        Array.Reverse(deltaQuantitesActions);
                        this._actionsSpinners = deltaQuantitesActions;
                        for (int i = 0; i < deltaQuantitesActions.Length; ++i)
                        {
                            if (deltaQuantitesActions[i] > 0)
                            {
                                this._actionsSpinners[i] = deltaQuantitesActions[i];
                                _actionsAI += AcheterAction(joueur.IdJoueur, joueur.NoJoueur);
                            }
                            else if (deltaQuantitesActions[i] < 0)
                            {
                                this._actionsSpinners[i] = deltaQuantitesActions[i] * -1;
                                _actionsAI += VendreAction(joueur.IdJoueur, joueur.NoJoueur);
                            }
                        }
                    }
                    else
                    {
                        _actionsAI += joueur.NomJoueur + " passe son tour.\n";
                    }
                    ReinitialiserActionSpinners();
                }
            }
        }
       
       /// <summary>
       /// Fonction d'achat du AI.
       /// </summary>
       /// <param name="NoJoueur"></param>
       /// <param name="IdJoueur"></param>
       /// <param name="deltaQuantitesActions"></param>
        private void AcheterAI(int noJoueur, IDJoueur IdJoueur, int[] deltaQuantitesActions)
        {
            this._actionsSpinners = deltaQuantitesActions;
            _actionsAI += AcheterAction(IdJoueur, noJoueur);
            ReinitialiserActionSpinners();
        }
        /// <summary>
        /// Fonction de vente du AI.
        /// </summary>
        /// <param name="noJoueur"></param>
        /// <param name="IdJoueur"></param>
        /// <param name="deltaQuantitesActions"></param>
         private void VendreAI(int noJoueur, IDJoueur IdJoueur, int[] deltaQuantitesActions)
        {
            _actionsAI = "";
            Array.Reverse(deltaQuantitesActions);
            this._actionsSpinners = deltaQuantitesActions;
            _actionsAI += VendreAction(IdJoueur, noJoueur);
            Console.WriteLine(_actionsAI);
            ReinitialiserActionSpinners();
        }
        /// <summary>
        /// Créer le tableau d'action du AI pour qu'il puisse fonctionner avec la fonction StockAI.
        /// </summary>
        /// <param name="joueurAI"></param>
        /// <returns>quantiteActions</returns>
        private int[] CreateTableAI(Joueur joueurAI)  
        {
	        int i = 0;
	        int[] quantiteActions = new int[6];
	        List <Action> liste = joueurAI.Actions;
	        foreach(Action act in joueurAI.Actions)
	        {
		        quantiteActions[i] = act.QteAction;
		        ++i;
	        }
            Array.Reverse(quantiteActions);
	        return quantiteActions;
        }
        /// <summary>
        /// Réinitialise la valeurs contenu dans les spinners.
        /// </summary>
        public void ReinitialiserActionSpinners()
        {
            for (int i = 0; i < this._actionsSpinners.Length; ++i )
            {
                this._actionsSpinners[i] = 0;
            }
        }
        /// <summary>
        /// Vérifie l'état des actions.
        /// </summary>
        /// <returns>msg</returns>
        public String VerifieValeursAction()
        {
            String msg = "";
            for(int i = 0; this._valeursActions.Length > i; ++i)
            {
                if (this._valeursActions[i].ValeurAction == 2)
                {
                    DedoublerActions(this._valeursActions[i].IdAction);
                    msg += "Dédoublement de l'action " + NOM_ACTIONS[i];
                }
                else if (this._valeursActions[i].ValeurAction == 0)
                {
                    PerdreActions(this._valeursActions[i].IdAction);
                    msg += "L'action " + NOM_ACTIONS[i] + "est off market!";
                }
            }
            return msg;
        }
        /// <summary>
        /// Dédouble les actions du monde qui en possède dans le cas où la valeur de l'action dépasse ou vaut 2$
        /// </summary>
        /// <param name="idAction"></param>
        private void DedoublerActions(IDActions idAction)
        {
            foreach (Joueur joueur in this._listeJoueurs)
            {
                if(joueur.Actions[(int)idAction].QteAction > 0)
                {
                    joueur.Actions[(int)idAction].QteAction *= 2;
                }
            }
        }
        /// <summary>
        /// Supprime toute les actions des joueurs qui en possède dans le cas d`un off the market.
        /// </summary>
        /// <param name="idAction"></param>
        private void PerdreActions(IDActions idAction)
        {
            foreach (Joueur joueur in this._listeJoueurs)
            {
                if (joueur.Actions[(int)idAction].QteAction > 0)
                {
                    joueur.Actions[(int)idAction].QteAction = 0;
                }
            }
        }
        private Des _des;
        
        private int _joueurActif;
        public int JoueurActif
        {
            get;
            set;
        }
        
        private int _toursFinis;
        public int ToursFinis
        {
            get { return _toursFinis; }
            set { _toursFinis = value; }
        }

        private int _nbreTours;
        public int NbreTours
        {
            get {return _nbreTours;}
            set { _nbreTours = value; }
        }

        private Action[] _valeursActions;
        public Action[] ValeursActions
        {
            get { return _valeursActions; }
            set { _valeursActions = value; }
        }

        private List<Joueur> _listeJoueurs;
        public List<Joueur> ListeJoueurs
        {
            get { return _listeJoueurs; }
            set {_listeJoueurs = value;}
        }

        private int[] _actionsSpinners;
        public int[] ActionsSpinners
        {
            get{return _actionsSpinners;}
            set { _actionsSpinners = value; }
        }
        private String _actionsAI;
        public String ActionsAI
        {
            get{return _actionsAI;}
            set{_actionsAI = value;}
        }
    }

}

