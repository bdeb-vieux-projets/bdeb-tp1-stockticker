﻿using System.Windows.Forms;
using System.Collections.Generic;
using System;
using System.Drawing;
namespace StockTicker
{
    partial class FenetreJeu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        /// 
        private int _nbreJoueurs;
        private System.ComponentModel.IContainer components = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent(int nbreJoueur, List<Joueur> listeJoueurs, Action[] valActions)
        {
            this._nbreJoueurs = nbreJoueur;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FenetreJeu));
            this.gbJoueurs = new System.Windows.Forms.GroupBox();
            this.tblScores = new System.Windows.Forms.TableLayoutPanel();
            this.lblMontant = new System.Windows.Forms.Label();           
            this.lblQteGrain = new System.Windows.Forms.Label();
            this.lblQteIndustrielles = new System.Windows.Forms.Label();
            this.lblQteBons = new System.Windows.Forms.Label();           
            this.lblQteArgent = new System.Windows.Forms.Label();
            this.lblQtePetrole = new System.Windows.Forms.Label();
            this.lblQteOr = new System.Windows.Forms.Label();
            this.gbResultDes = new System.Windows.Forms.GroupBox();
            this.lblResultsDes = new System.Windows.Forms.Label();
            this.btnRoulerDes = new System.Windows.Forms.Button();
            this.mnuJeu = new System.Windows.Forms.MenuStrip();
            this.jeuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.paramsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nouvellePartieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aideToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.àProposToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gbActions = new System.Windows.Forms.GroupBox();
            this.numOr = new System.Windows.Forms.NumericUpDown();
            this.numArgent = new System.Windows.Forms.NumericUpDown();
            this.numPetrole = new System.Windows.Forms.NumericUpDown();
            this.numBons = new System.Windows.Forms.NumericUpDown();
            this.btnAcheter = new System.Windows.Forms.Button();
            this.btnVendre = new System.Windows.Forms.Button();
            this.numGrains = new System.Windows.Forms.NumericUpDown();
            this.numIndustrielles = new System.Windows.Forms.NumericUpDown();
            this.lblActionGrains = new System.Windows.Forms.Label();
            this.lblActionIndustrielles = new System.Windows.Forms.Label();
            this.lblActionBons = new System.Windows.Forms.Label();
            this.lblActionPetrole = new System.Windows.Forms.Label();
            this.lblActionArgent = new System.Windows.Forms.Label();
            this.lblActionOr = new System.Windows.Forms.Label();
            this.tblFluctuationsAct = new System.Windows.Forms.TableLayoutPanel();
            this.lblPosIndustrielles = new System.Windows.Forms.Label();
            this.lblPosBons = new System.Windows.Forms.Label();
            this.lblPosPetrole = new System.Windows.Forms.Label();
            this.lblPosArgent = new System.Windows.Forms.Label();
            this.lblPosOr = new System.Windows.Forms.Label();
            this.lblFlucOr = new System.Windows.Forms.Label();
            this.lblFlucPetrole = new System.Windows.Forms.Label();
            this.lblFlucBons = new System.Windows.Forms.Label();
            this.lblFlucIndustrielles = new System.Windows.Forms.Label();
            this.lblFlucGrains = new System.Windows.Forms.Label();
            this.lblValOr = new System.Windows.Forms.Label();
            this.lblValArgent = new System.Windows.Forms.Label();
            this.lblValPetrole = new System.Windows.Forms.Label();
            this.lblValBons = new System.Windows.Forms.Label();
            this.lblValIndustrielles = new System.Windows.Forms.Label();
            this.lblValGrains = new System.Windows.Forms.Label();
            this.lblGrains = new System.Windows.Forms.Label();
            this.lblIndustrielles = new System.Windows.Forms.Label();
            this.lblBons = new System.Windows.Forms.Label();
            this.lblPetrole = new System.Windows.Forms.Label();
            this.lblArgent = new System.Windows.Forms.Label();
            this.lblOr = new System.Windows.Forms.Label();
            this.lblFlucArgent = new System.Windows.Forms.Label();
            this.lblPosGrain = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tlsTours = new System.Windows.Forms.ToolStripStatusLabel();
            this.tlsToursComplet = new System.Windows.Forms.ToolStripStatusLabel();
            this.gbHistorique = new System.Windows.Forms.GroupBox();
            this.txtHistorique = new System.Windows.Forms.TextBox();
            this.gbJoueurs.SuspendLayout();
            this.tblScores.SuspendLayout();
            this.gbResultDes.SuspendLayout();
            this.mnuJeu.SuspendLayout();
            this.gbActions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numOr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numArgent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPetrole)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numBons)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numGrains)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numIndustrielles)).BeginInit();
            this.tblFluctuationsAct.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.gbHistorique.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbJoueurs
            // 
            this.gbJoueurs.Controls.Add(this.tblScores);
            this.gbJoueurs.Location = new System.Drawing.Point(12, 93);
            this.gbJoueurs.Name = "gbJoueurs";
            this.gbJoueurs.Size = new System.Drawing.Size(442, 191);
            this.gbJoueurs.TabIndex = 0;
            this.gbJoueurs.TabStop = false;
            this.gbJoueurs.Text = "Joueurs:";
            // 
            // tblScores
            // 
            
            // 
            // lblMontant
            // 
            this.lblMontant.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMontant.AutoSize = true;
            this.lblMontant.BackColor = System.Drawing.SystemColors.Control;
            this.lblMontant.Location = new System.Drawing.Point(1, 20);
            this.lblMontant.Margin = new System.Windows.Forms.Padding(0);
            this.lblMontant.Name = "lblMontant";
            this.lblMontant.Size = new System.Drawing.Size(84, 18);
            this.lblMontant.TabIndex = 35;
            this.lblMontant.Text = "$$$";
            this.lblMontant.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
          
            // 
            // lblQteGrain
            // 
            this.lblQteGrain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblQteGrain.AutoSize = true;
            this.lblQteGrain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblQteGrain.Location = new System.Drawing.Point(1, 138);
            this.lblQteGrain.Margin = new System.Windows.Forms.Padding(0);
            this.lblQteGrain.Name = "lblQteGrain";
            this.lblQteGrain.Size = new System.Drawing.Size(84, 23);
            this.lblQteGrain.TabIndex = 31;
            this.lblQteGrain.Text = "Grain";
            this.lblQteGrain.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblQteIndustrielles
            // 
            this.lblQteIndustrielles.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblQteIndustrielles.AutoSize = true;
            this.lblQteIndustrielles.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.lblQteIndustrielles.Location = new System.Drawing.Point(1, 117);
            this.lblQteIndustrielles.Margin = new System.Windows.Forms.Padding(0);
            this.lblQteIndustrielles.Name = "lblQteIndustrielles";
            this.lblQteIndustrielles.Size = new System.Drawing.Size(84, 20);
            this.lblQteIndustrielles.TabIndex = 9;
            this.lblQteIndustrielles.Text = "Industrielles";
            this.lblQteIndustrielles.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblQteBons
            // 
            this.lblQteBons.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblQteBons.AutoSize = true;
            this.lblQteBons.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblQteBons.Location = new System.Drawing.Point(1, 96);
            this.lblQteBons.Margin = new System.Windows.Forms.Padding(0);
            this.lblQteBons.Name = "lblQteBons";
            this.lblQteBons.Size = new System.Drawing.Size(84, 20);
            this.lblQteBons.TabIndex = 8;
            this.lblQteBons.Text = "Bons";
            this.lblQteArgent.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
           
            // 
            // lblQteArgent
            // 
            this.lblQteArgent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblQteArgent.AutoSize = true;
            this.lblQteArgent.BackColor = System.Drawing.Color.Silver;
            this.lblQteArgent.Location = new System.Drawing.Point(1, 77);
            this.lblQteArgent.Margin = new System.Windows.Forms.Padding(0);
            this.lblQteArgent.Name = "lblQteArgent";
            this.lblQteArgent.Size = new System.Drawing.Size(84, 18);
            this.lblQteArgent.TabIndex = 6;
            this.lblQteArgent.Text = "Argent";
            this.lblQteArgent.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblQtePetrole
            // 
            this.lblQtePetrole.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblQtePetrole.AutoSize = true;
            this.lblQtePetrole.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.lblQtePetrole.Location = new System.Drawing.Point(1, 58);
            this.lblQtePetrole.Margin = new System.Windows.Forms.Padding(0);
            this.lblQtePetrole.Name = "lblQtePetrole";
            this.lblQtePetrole.Size = new System.Drawing.Size(84, 18);
            this.lblQtePetrole.TabIndex = 7;
            this.lblQtePetrole.Text = "Pétroles";
            this.lblQtePetrole.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
           
            // 
            // lblQteOr
            // 
            this.lblQteOr.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblQteOr.AutoSize = true;
            this.lblQteOr.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lblQteOr.Location = new System.Drawing.Point(1, 39);
            this.lblQteOr.Margin = new System.Windows.Forms.Padding(0);
            this.lblQteOr.Name = "lblQteOr";
            this.lblQteOr.Size = new System.Drawing.Size(84, 18);
            this.lblQteOr.TabIndex = 5;
            this.lblQteOr.Text = "Or";
            this.lblQteOr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // gbResultDes
            // 
            this.gbResultDes.Controls.Add(this.lblResultsDes);
            this.gbResultDes.Controls.Add(this.btnRoulerDes);
            this.gbResultDes.Location = new System.Drawing.Point(12, 27);
            this.gbResultDes.Name = "gbResultDes";
            this.gbResultDes.Size = new System.Drawing.Size(442, 60);
            this.gbResultDes.TabIndex = 1;
            this.gbResultDes.TabStop = false;
            this.gbResultDes.Text = "Résultat des dés:";
            // 
            // lblResultsDes
            // 
            this.lblResultsDes.AutoSize = true;
            this.lblResultsDes.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResultsDes.Location = new System.Drawing.Point(45, 21);
            this.lblResultsDes.Name = "lblResultsDes";
            this.lblResultsDes.Size = new System.Drawing.Size(0, 24);
            this.lblResultsDes.TabIndex = 0;
            // 
            // btnRoulerDes
            // 
            this.btnRoulerDes.Font = new System.Drawing.Font("Comic Sans MS", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRoulerDes.Image = ((System.Drawing.Image)(resources.GetObject("btnRoulerDes.Image")));
            this.btnRoulerDes.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRoulerDes.Location = new System.Drawing.Point(304, 17);
            this.btnRoulerDes.Name = "btnRoulerDes";
            this.btnRoulerDes.Size = new System.Drawing.Size(120, 37);
            this.btnRoulerDes.TabIndex = 4;
            this.btnRoulerDes.Text = "Rouler les dés";
            this.btnRoulerDes.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnRoulerDes.UseVisualStyleBackColor = true;
            this.btnRoulerDes.Click += new System.EventHandler(this.btnRoulerDes_Click);
            // 
            // mnuJeu
            // 
            this.mnuJeu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.jeuToolStripMenuItem,
            this.aideToolStripMenuItem});
            this.mnuJeu.Location = new System.Drawing.Point(0, 0);
            this.mnuJeu.Name = "mnuJeu";
            this.mnuJeu.Size = new System.Drawing.Size(1076, 24);
            this.mnuJeu.TabIndex = 6;
            this.mnuJeu.Text = "menuStrip1";
            // 
            // jeuToolStripMenuItem
            // 
            this.jeuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
                this.nouvellePartieToolStripMenuItem,
                this.paramsToolStripMenuItem,                
                this.quitterToolStripMenuItem});

            this.jeuToolStripMenuItem.Name = "jeuToolStripMenuItem";
            this.jeuToolStripMenuItem.Size = new System.Drawing.Size(36, 20);
            this.jeuToolStripMenuItem.Text = "&Jeu";

            this.paramsToolStripMenuItem.Name = "paramsToolStripMenuItem";
            this.paramsToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.paramsToolStripMenuItem.Text = "&Options...";
            this.paramsToolStripMenuItem.Click += new System.EventHandler(this.paramsToolStripMenuItem_Click);
            // 
            // nouvellePartieToolStripMenuItem
            // 
            this.nouvellePartieToolStripMenuItem.Name = "nouvellePartieToolStripMenuItem";
            this.nouvellePartieToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.nouvellePartieToolStripMenuItem.Text = "&Nouvelle partie";
            this.nouvellePartieToolStripMenuItem.Click += new System.EventHandler(this.nouvellePartieToolStripMenuItem_Click);
            // 
            // quitterToolStripMenuItem
            // 
            this.quitterToolStripMenuItem.Name = "quitterToolStripMenuItem";
            this.quitterToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.quitterToolStripMenuItem.Text = "&Quitter";
            this.quitterToolStripMenuItem.Click += new System.EventHandler(this.quitterToolStripMenuItem_Click);
                //this.btnVendre.Click += new System.EventHandler(this.btnVendre_Click);
            // 
            // aideToolStripMenuItem
            // 
            this.aideToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.àProposToolStripMenuItem});
            this.aideToolStripMenuItem.Name = "aideToolStripMenuItem";
            this.aideToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.aideToolStripMenuItem.Text = "&Aide";
            // 
            // àProposToolStripMenuItem
            // 
            this.àProposToolStripMenuItem.Name = "àProposToolStripMenuItem";
            this.àProposToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.àProposToolStripMenuItem.Text = "&À propos...";
            // 
            // gbActions
            // 
            this.gbActions.Controls.Add(this.numOr);
            this.gbActions.Controls.Add(this.numArgent);
            this.gbActions.Controls.Add(this.numPetrole);
            this.gbActions.Controls.Add(this.numBons);
            this.gbActions.Controls.Add(this.btnAcheter);
            this.gbActions.Controls.Add(this.btnVendre);
            this.gbActions.Controls.Add(this.numGrains);
            this.gbActions.Controls.Add(this.numIndustrielles);
            this.gbActions.Controls.Add(this.lblActionGrains);
            this.gbActions.Controls.Add(this.lblActionIndustrielles);
            this.gbActions.Controls.Add(this.lblActionBons);
            this.gbActions.Controls.Add(this.lblActionPetrole);
            this.gbActions.Controls.Add(this.lblActionArgent);
            this.gbActions.Controls.Add(this.lblActionOr);
            this.gbActions.Location = new System.Drawing.Point(12, 290);
            this.gbActions.Name = "gbActions";
            this.gbActions.Size = new System.Drawing.Size(442, 181);
            this.gbActions.TabIndex = 7;
            this.gbActions.TabStop = false;
            this.gbActions.Text = "Actions:";
            // 
            // numOr
            // 
            this.numOr.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numOr.Location = new System.Drawing.Point(180, 24);
            this.numOr.Maximum = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            this.numOr.Name = "numOr";
            this.numOr.Size = new System.Drawing.Size(67, 20);
            this.numOr.TabIndex = 11;
            this.numOr.ValueChanged += new System.EventHandler(this.numOr_ValueChanged);
            // 
            // numArgent
            // 
            this.numArgent.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numArgent.Location = new System.Drawing.Point(180, 50);
            this.numArgent.Maximum = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            this.numArgent.Name = "numArgent";
            this.numArgent.Size = new System.Drawing.Size(67, 20);
            this.numArgent.TabIndex = 10;
            this.numArgent.ValueChanged += new System.EventHandler(this.numArgent_ValueChanged);
            // 
            // numPetrole
            // 
            this.numPetrole.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numPetrole.Location = new System.Drawing.Point(180, 76);
            this.numPetrole.Maximum = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            this.numPetrole.Name = "numPetrole";
            this.numPetrole.Size = new System.Drawing.Size(67, 20);
            this.numPetrole.TabIndex = 9;
            this.numPetrole.ValueChanged += new System.EventHandler(this.numPetrole_ValueChanged);
            // 
            // numBons
            // 
            this.numBons.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numBons.Location = new System.Drawing.Point(180, 102);
            this.numBons.Maximum = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            this.numBons.Name = "numBons";
            this.numBons.Size = new System.Drawing.Size(67, 20);
            this.numBons.TabIndex = 8;
            this.numBons.ValueChanged += new System.EventHandler(this.numBons_ValueChanged);
            // 
            // btnAcheter
            // 
            this.btnAcheter.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAcheter.Image = global::StockTicker.Properties.Resources.plus;
            this.btnAcheter.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAcheter.Location = new System.Drawing.Point(253, 24);
            this.btnAcheter.Name = "btnAcheter";
            this.btnAcheter.Size = new System.Drawing.Size(171, 72);
            this.btnAcheter.TabIndex = 3;
            this.btnAcheter.Text = "Acheter";
            this.btnAcheter.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAcheter.UseVisualStyleBackColor = true;
            this.btnAcheter.Click += new System.EventHandler(this.btnAcheter_Click);
            // 
            // btnVendre
            // 
            this.btnVendre.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Bold);
            this.btnVendre.Image = global::StockTicker.Properties.Resources.moins;
            this.btnVendre.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnVendre.Location = new System.Drawing.Point(253, 102);
            this.btnVendre.Name = "btnVendre";
            this.btnVendre.Size = new System.Drawing.Size(171, 72);
            this.btnVendre.TabIndex = 2;
            this.btnVendre.Text = "Vendre";
            this.btnVendre.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnVendre.UseVisualStyleBackColor = true;
            this.btnVendre.Click += new System.EventHandler(this.btnVendre_Click);
            // 
            // numGrains
            // 
            this.numGrains.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numGrains.Location = new System.Drawing.Point(180, 154);
            this.numGrains.Maximum = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            this.numGrains.Name = "numGrains";
            this.numGrains.Size = new System.Drawing.Size(67, 20);
            this.numGrains.TabIndex = 7;
            this.numGrains.ValueChanged += new System.EventHandler(this.numGrains_ValueChanged);
            // 
            // numIndustrielles
            // 
            this.numIndustrielles.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numIndustrielles.Location = new System.Drawing.Point(180, 128);
            this.numIndustrielles.Maximum = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            this.numIndustrielles.Name = "numIndustrielles";
            this.numIndustrielles.Size = new System.Drawing.Size(67, 20);
            this.numIndustrielles.TabIndex = 6;
            this.numIndustrielles.ValueChanged += new System.EventHandler(this.numIndustrielles_ValueChanged);
            // 
            // lblActionGrains
            // 
            this.lblActionGrains.AutoSize = true;
            this.lblActionGrains.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblActionGrains.Location = new System.Drawing.Point(14, 156);
            this.lblActionGrains.Name = "lblActionGrains";
            this.lblActionGrains.Size = new System.Drawing.Size(40, 13);
            this.lblActionGrains.TabIndex = 5;
            this.lblActionGrains.Text = "Grains:";
            // 
            // lblActionIndustrielles
            // 
            this.lblActionIndustrielles.AutoSize = true;
            this.lblActionIndustrielles.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.lblActionIndustrielles.Location = new System.Drawing.Point(14, 130);
            this.lblActionIndustrielles.Name = "lblActionIndustrielles";
            this.lblActionIndustrielles.Size = new System.Drawing.Size(65, 13);
            this.lblActionIndustrielles.TabIndex = 4;
            this.lblActionIndustrielles.Text = "Industrielles:";
            // 
            // lblActionBons
            // 
            this.lblActionBons.AutoSize = true;
            this.lblActionBons.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblActionBons.Location = new System.Drawing.Point(14, 104);
            this.lblActionBons.Name = "lblActionBons";
            this.lblActionBons.Size = new System.Drawing.Size(34, 13);
            this.lblActionBons.TabIndex = 3;
            this.lblActionBons.Text = "Bons:";
            // 
            // lblActionPetrole
            // 
            this.lblActionPetrole.AutoSize = true;
            this.lblActionPetrole.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.lblActionPetrole.Location = new System.Drawing.Point(13, 78);
            this.lblActionPetrole.Name = "lblActionPetrole";
            this.lblActionPetrole.Size = new System.Drawing.Size(48, 13);
            this.lblActionPetrole.TabIndex = 2;
            this.lblActionPetrole.Text = "Pétroles:";
            // 
            // lblActionArgent
            // 
            this.lblActionArgent.AutoSize = true;
            this.lblActionArgent.BackColor = System.Drawing.Color.Silver;
            this.lblActionArgent.Location = new System.Drawing.Point(13, 52);
            this.lblActionArgent.Name = "lblActionArgent";
            this.lblActionArgent.Size = new System.Drawing.Size(41, 13);
            this.lblActionArgent.TabIndex = 1;
            this.lblActionArgent.Text = "Argent:";
            // 
            // lblActionOr
            // 
            this.lblActionOr.AutoSize = true;
            this.lblActionOr.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lblActionOr.Location = new System.Drawing.Point(14, 26);
            this.lblActionOr.Name = "lblActionOr";
            this.lblActionOr.Size = new System.Drawing.Size(21, 13);
            this.lblActionOr.TabIndex = 0;
            this.lblActionOr.Text = "Or:";

            InitialiseTableScores();
            // 
            // tblFluctuationsAct
            // 
            this.tblFluctuationsAct.ColumnCount = 4;
            this.tblFluctuationsAct.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.00062F));
            this.tblFluctuationsAct.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.00062F));
            this.tblFluctuationsAct.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.00062F));
            this.tblFluctuationsAct.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.99813F));
            this.tblFluctuationsAct.Controls.Add(this.lblPosIndustrielles, 0, 4);
            this.tblFluctuationsAct.Controls.Add(this.lblPosBons, 0, 3);
            this.tblFluctuationsAct.Controls.Add(this.lblPosPetrole, 0, 2);
            this.tblFluctuationsAct.Controls.Add(this.lblPosArgent, 0, 1);
            this.tblFluctuationsAct.Controls.Add(this.lblPosOr, 0, 0);
            this.tblFluctuationsAct.Controls.Add(this.lblFlucOr, 3, 0);
            this.tblFluctuationsAct.Controls.Add(this.lblFlucPetrole, 3, 2);
            this.tblFluctuationsAct.Controls.Add(this.lblFlucBons, 3, 3);
            this.tblFluctuationsAct.Controls.Add(this.lblFlucIndustrielles, 3, 4);
            this.tblFluctuationsAct.Controls.Add(this.lblFlucGrains, 3, 5);
            this.tblFluctuationsAct.Controls.Add(this.lblValOr, 2, 0);
            this.tblFluctuationsAct.Controls.Add(this.lblValArgent, 2, 1);
            this.tblFluctuationsAct.Controls.Add(this.lblValPetrole, 2, 2);
            this.tblFluctuationsAct.Controls.Add(this.lblValBons, 2, 3);
            this.tblFluctuationsAct.Controls.Add(this.lblValIndustrielles, 2, 4);
            this.tblFluctuationsAct.Controls.Add(this.lblValGrains, 2, 5);
            this.tblFluctuationsAct.Controls.Add(this.lblGrains, 1, 5);
            this.tblFluctuationsAct.Controls.Add(this.lblIndustrielles, 1, 4);
            this.tblFluctuationsAct.Controls.Add(this.lblBons, 1, 3);
            this.tblFluctuationsAct.Controls.Add(this.lblPetrole, 1, 2);
            this.tblFluctuationsAct.Controls.Add(this.lblArgent, 1, 1);
            this.tblFluctuationsAct.Controls.Add(this.lblOr, 1, 0);
            this.tblFluctuationsAct.Controls.Add(this.lblFlucArgent, 3, 1);
            this.tblFluctuationsAct.Controls.Add(this.lblPosGrain, 0, 5);
            this.tblFluctuationsAct.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.tblFluctuationsAct.Location = new System.Drawing.Point(457, 27);
            this.tblFluctuationsAct.Margin = new System.Windows.Forms.Padding(0);
            this.tblFluctuationsAct.Name = "tblFluctuationsAct";
            this.tblFluctuationsAct.RowCount = 6;
            this.tblFluctuationsAct.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tblFluctuationsAct.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tblFluctuationsAct.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tblFluctuationsAct.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tblFluctuationsAct.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tblFluctuationsAct.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tblFluctuationsAct.Size = new System.Drawing.Size(610, 444);
            this.tblFluctuationsAct.TabIndex = 8;
            // 
            // lblPosIndustrielles
            // 
            this.lblPosIndustrielles.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPosIndustrielles.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.lblPosIndustrielles.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lblPosIndustrielles.Location = new System.Drawing.Point(0, 292);
            this.lblPosIndustrielles.Margin = new System.Windows.Forms.Padding(0);
            this.lblPosIndustrielles.Name = "lblPosIndustrielles";
            this.lblPosIndustrielles.Size = new System.Drawing.Size(152, 73);
            this.lblPosIndustrielles.TabIndex = 22;
            this.lblPosIndustrielles.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPosBons
            // 
            this.lblPosBons.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPosBons.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblPosBons.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lblPosBons.Location = new System.Drawing.Point(0, 219);
            this.lblPosBons.Margin = new System.Windows.Forms.Padding(0);
            this.lblPosBons.Name = "lblPosBons";
            this.lblPosBons.Size = new System.Drawing.Size(152, 73);
            this.lblPosBons.TabIndex = 21;
            this.lblPosBons.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPosPetrole
            // 
            this.lblPosPetrole.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPosPetrole.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.lblPosPetrole.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lblPosPetrole.Location = new System.Drawing.Point(0, 146);
            this.lblPosPetrole.Margin = new System.Windows.Forms.Padding(0);
            this.lblPosPetrole.Name = "lblPosPetrole";
            this.lblPosPetrole.Size = new System.Drawing.Size(152, 73);
            this.lblPosPetrole.TabIndex = 20;
            this.lblPosPetrole.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPosArgent
            // 
            this.lblPosArgent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPosArgent.BackColor = System.Drawing.Color.Silver;
            this.lblPosArgent.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lblPosArgent.Location = new System.Drawing.Point(0, 73);
            this.lblPosArgent.Margin = new System.Windows.Forms.Padding(0);
            this.lblPosArgent.Name = "lblPosArgent";
            this.lblPosArgent.Size = new System.Drawing.Size(152, 73);
            this.lblPosArgent.TabIndex = 19;
            this.lblPosArgent.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPosOr
            // 
            this.lblPosOr.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPosOr.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lblPosOr.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lblPosOr.Location = new System.Drawing.Point(0, 0);
            this.lblPosOr.Margin = new System.Windows.Forms.Padding(0);
            this.lblPosOr.Name = "lblPosOr";
            this.lblPosOr.Size = new System.Drawing.Size(152, 73);
            this.lblPosOr.TabIndex = 18;
            this.lblPosOr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblFlucOr
            // 
            this.lblFlucOr.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFlucOr.BackColor = System.Drawing.SystemColors.Control;
            this.lblFlucOr.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lblFlucOr.Location = new System.Drawing.Point(456, 0);
            this.lblFlucOr.Margin = new System.Windows.Forms.Padding(0);
            this.lblFlucOr.Name = "lblFlucOr";
            this.lblFlucOr.Size = new System.Drawing.Size(154, 73);
            this.lblFlucOr.TabIndex = 2;
            this.lblFlucOr.Text = "0";
            this.lblFlucOr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblFlucPetrole
            // 
            this.lblFlucPetrole.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFlucPetrole.BackColor = System.Drawing.SystemColors.Control;
            this.lblFlucPetrole.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lblFlucPetrole.Location = new System.Drawing.Point(456, 146);
            this.lblFlucPetrole.Margin = new System.Windows.Forms.Padding(0);
            this.lblFlucPetrole.Name = "lblFlucPetrole";
            this.lblFlucPetrole.Size = new System.Drawing.Size(154, 73);
            this.lblFlucPetrole.TabIndex = 8;
            this.lblFlucPetrole.Text = "0";
            this.lblFlucPetrole.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblFlucBons
            // 
            this.lblFlucBons.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFlucBons.BackColor = System.Drawing.SystemColors.Control;
            this.lblFlucBons.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lblFlucBons.Location = new System.Drawing.Point(456, 219);
            this.lblFlucBons.Margin = new System.Windows.Forms.Padding(0);
            this.lblFlucBons.Name = "lblFlucBons";
            this.lblFlucBons.Size = new System.Drawing.Size(154, 73);
            this.lblFlucBons.TabIndex = 11;
            this.lblFlucBons.Text = "0";
            this.lblFlucBons.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblFlucIndustrielles
            // 
            this.lblFlucIndustrielles.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFlucIndustrielles.BackColor = System.Drawing.SystemColors.Control;
            this.lblFlucIndustrielles.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lblFlucIndustrielles.Location = new System.Drawing.Point(456, 292);
            this.lblFlucIndustrielles.Margin = new System.Windows.Forms.Padding(0);
            this.lblFlucIndustrielles.Name = "lblFlucIndustrielles";
            this.lblFlucIndustrielles.Size = new System.Drawing.Size(154, 73);
            this.lblFlucIndustrielles.TabIndex = 14;
            this.lblFlucIndustrielles.Text = "0";
            this.lblFlucIndustrielles.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblFlucGrains
            // 
            this.lblFlucGrains.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFlucGrains.BackColor = System.Drawing.SystemColors.Control;
            this.lblFlucGrains.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lblFlucGrains.Location = new System.Drawing.Point(456, 365);
            this.lblFlucGrains.Margin = new System.Windows.Forms.Padding(0);
            this.lblFlucGrains.Name = "lblFlucGrains";
            this.lblFlucGrains.Size = new System.Drawing.Size(154, 79);
            this.lblFlucGrains.TabIndex = 17;
            this.lblFlucGrains.Text = "0";
            this.lblFlucGrains.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblValOr
            // 
            this.lblValOr.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblValOr.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lblValOr.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lblValOr.Location = new System.Drawing.Point(304, 0);
            this.lblValOr.Margin = new System.Windows.Forms.Padding(0);
            this.lblValOr.Name = "lblValOr";
            this.lblValOr.Size = new System.Drawing.Size(152, 73);
            this.lblValOr.TabIndex = 1;
            this.lblValOr.Text = "0";
            this.lblValOr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblValArgent
            // 
            this.lblValArgent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblValArgent.BackColor = System.Drawing.Color.Silver;
            this.lblValArgent.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lblValArgent.Location = new System.Drawing.Point(304, 73);
            this.lblValArgent.Margin = new System.Windows.Forms.Padding(0);
            this.lblValArgent.Name = "lblValArgent";
            this.lblValArgent.Size = new System.Drawing.Size(152, 73);
            this.lblValArgent.TabIndex = 4;
            this.lblValArgent.Text = "0";
            this.lblValArgent.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblValPetrole
            // 
            this.lblValPetrole.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblValPetrole.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.lblValPetrole.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lblValPetrole.Location = new System.Drawing.Point(304, 146);
            this.lblValPetrole.Margin = new System.Windows.Forms.Padding(0);
            this.lblValPetrole.Name = "lblValPetrole";
            this.lblValPetrole.Size = new System.Drawing.Size(152, 73);
            this.lblValPetrole.TabIndex = 7;
            this.lblValPetrole.Text = "0";
            this.lblValPetrole.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblValBons
            // 
            this.lblValBons.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblValBons.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblValBons.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lblValBons.Location = new System.Drawing.Point(304, 219);
            this.lblValBons.Margin = new System.Windows.Forms.Padding(0);
            this.lblValBons.Name = "lblValBons";
            this.lblValBons.Size = new System.Drawing.Size(152, 73);
            this.lblValBons.TabIndex = 10;
            this.lblValBons.Text = "0";
            this.lblValBons.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblValIndustrielles
            // 
            this.lblValIndustrielles.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblValIndustrielles.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.lblValIndustrielles.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lblValIndustrielles.Location = new System.Drawing.Point(304, 292);
            this.lblValIndustrielles.Margin = new System.Windows.Forms.Padding(0);
            this.lblValIndustrielles.Name = "lblValIndustrielles";
            this.lblValIndustrielles.Size = new System.Drawing.Size(152, 73);
            this.lblValIndustrielles.TabIndex = 13;
            this.lblValIndustrielles.Text = "0";
            this.lblValIndustrielles.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblValGrains
            // 
            this.lblValGrains.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblValGrains.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblValGrains.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lblValGrains.Location = new System.Drawing.Point(304, 365);
            this.lblValGrains.Margin = new System.Windows.Forms.Padding(0);
            this.lblValGrains.Name = "lblValGrains";
            this.lblValGrains.Size = new System.Drawing.Size(152, 79);
            this.lblValGrains.TabIndex = 16;
            this.lblValGrains.Text = "0";
            this.lblValGrains.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblGrains
            // 
            this.lblGrains.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblGrains.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblGrains.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lblGrains.Location = new System.Drawing.Point(152, 365);
            this.lblGrains.Margin = new System.Windows.Forms.Padding(0);
            this.lblGrains.Name = "lblGrains";
            this.lblGrains.Size = new System.Drawing.Size(152, 79);
            this.lblGrains.TabIndex = 15;
            this.lblGrains.Text = "Grains";
            this.lblGrains.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblIndustrielles
            // 
            this.lblIndustrielles.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblIndustrielles.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.lblIndustrielles.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lblIndustrielles.Location = new System.Drawing.Point(152, 292);
            this.lblIndustrielles.Margin = new System.Windows.Forms.Padding(0);
            this.lblIndustrielles.Name = "lblIndustrielles";
            this.lblIndustrielles.Size = new System.Drawing.Size(152, 73);
            this.lblIndustrielles.TabIndex = 12;
            this.lblIndustrielles.Text = "Industrielles";
            this.lblIndustrielles.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblBons
            // 
            this.lblBons.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblBons.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblBons.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lblBons.Location = new System.Drawing.Point(152, 219);
            this.lblBons.Margin = new System.Windows.Forms.Padding(0);
            this.lblBons.Name = "lblBons";
            this.lblBons.Size = new System.Drawing.Size(152, 73);
            this.lblBons.TabIndex = 9;
            this.lblBons.Text = "Bons";
            this.lblBons.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPetrole
            // 
            this.lblPetrole.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPetrole.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.lblPetrole.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lblPetrole.Location = new System.Drawing.Point(152, 146);
            this.lblPetrole.Margin = new System.Windows.Forms.Padding(0);
            this.lblPetrole.Name = "lblPetrole";
            this.lblPetrole.Size = new System.Drawing.Size(152, 73);
            this.lblPetrole.TabIndex = 6;
            this.lblPetrole.Text = "Pétroles";
            this.lblPetrole.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblArgent
            // 
            this.lblArgent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblArgent.BackColor = System.Drawing.Color.Silver;
            this.lblArgent.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lblArgent.Location = new System.Drawing.Point(152, 73);
            this.lblArgent.Margin = new System.Windows.Forms.Padding(0);
            this.lblArgent.Name = "lblArgent";
            this.lblArgent.Size = new System.Drawing.Size(152, 73);
            this.lblArgent.TabIndex = 3;
            this.lblArgent.Text = "Argent";
            this.lblArgent.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblOr
            // 
            this.lblOr.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblOr.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lblOr.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lblOr.Location = new System.Drawing.Point(152, 0);
            this.lblOr.Margin = new System.Windows.Forms.Padding(0);
            this.lblOr.Name = "lblOr";
            this.lblOr.Size = new System.Drawing.Size(152, 73);
            this.lblOr.Text = "Or";
            this.lblOr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblFlucArgent
            // 
            this.lblFlucArgent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFlucArgent.BackColor = System.Drawing.SystemColors.Control;
            this.lblFlucArgent.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lblFlucArgent.Location = new System.Drawing.Point(456, 73);
            this.lblFlucArgent.Margin = new System.Windows.Forms.Padding(0);
            this.lblFlucArgent.Name = "lblFlucArgent";
            this.lblFlucArgent.Size = new System.Drawing.Size(154, 73);
            this.lblFlucArgent.TabIndex = 5;
            this.lblFlucArgent.Text = "0";
            this.lblFlucArgent.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPosGrain
            // 
            this.lblPosGrain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPosGrain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblPosGrain.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lblPosGrain.Location = new System.Drawing.Point(0, 365);
            this.lblPosGrain.Margin = new System.Windows.Forms.Padding(0);
            this.lblPosGrain.Name = "lblPosGrain";
            this.lblPosGrain.Size = new System.Drawing.Size(152, 79);
            this.lblPosGrain.TabIndex = 23;
            this.lblPosGrain.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tlsTours,
            this.tlsToursComplet});
            this.statusStrip1.Location = new System.Drawing.Point(0, 626);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1076, 22);
            this.statusStrip1.SizingGrip = false;
            this.statusStrip1.TabIndex = 9;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tlsTours
            // 
            this.tlsTours.Name = "tlsTours";
            this.tlsTours.Size = new System.Drawing.Size(43, 17);
            this.tlsTours.Text = "Tours: ";
            // 
            // tlsToursComplet
            // 
            this.tlsToursComplet.Name = "tlsToursComplet";
            this.tlsToursComplet.Size = new System.Drawing.Size(0, 17);
            // 
            // gbHistorique
            // 
            this.gbHistorique.Controls.Add(this.txtHistorique);
            this.gbHistorique.Location = new System.Drawing.Point(12, 480);
            this.gbHistorique.Name = "gbHistorique";
            this.gbHistorique.Size = new System.Drawing.Size(1055, 138);
            this.gbHistorique.TabIndex = 10;
            this.gbHistorique.TabStop = false;
            this.gbHistorique.Text = "Historique:";
            // 
            // txtHistorique
            // 
            this.txtHistorique.Location = new System.Drawing.Point(6, 19);
            this.txtHistorique.Multiline = true;
            this.txtHistorique.Name = "txtHistorique";
            this.txtHistorique.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtHistorique.Size = new System.Drawing.Size(1043, 113);
            this.txtHistorique.TabIndex = 0;
            // 
            // FenetreJeu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::StockTicker.Properties.Resources.stock_market_quotes;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1076, 648);
            this.Controls.Add(this.gbHistorique);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.gbJoueurs);
            this.Controls.Add(this.tblFluctuationsAct);
            this.Controls.Add(this.gbActions);
            this.Controls.Add(this.gbResultDes);
            this.Controls.Add(this.mnuJeu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MainMenuStrip = this.mnuJeu;
            this.MaximizeBox = false;
            this.Name = "FenetreJeu";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Stock ticker";
            this.Validated += new System.EventHandler(this.FenetreJeu_Validated);
            this.gbJoueurs.ResumeLayout(false);
            this.tblScores.ResumeLayout(false);
            this.tblScores.PerformLayout();
            this.gbResultDes.ResumeLayout(false);
            this.gbResultDes.PerformLayout();
            this.mnuJeu.ResumeLayout(false);
            this.mnuJeu.PerformLayout();
            this.gbActions.ResumeLayout(false);
            this.gbActions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numOr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numArgent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPetrole)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numBons)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numGrains)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numIndustrielles)).EndInit();
            this.tblFluctuationsAct.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.gbHistorique.ResumeLayout(false);
            this.gbHistorique.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();
            UpdateTableActions(valActions);
            CreateTableScores(nbreJoueur, listeJoueurs);
        }
        /// <summary>
        /// Fonction qui créer la table des scores selon le nombre de joueurs et les infos des joueurs.
        /// </summary>
        /// <param name="nbreJoueurs"></param>
        /// <param name="listeJoueurs"></param>
        private void CreateTableScores(int nbreJoueurs, List <Joueur> listeJoueurs)
        {
            int position = 86,
                i;
            Label lab;
            this.lblNomsJoueurs = new List<Label>();
            this.lblMontantJoueurs = new List<Label>();
            this.lblOrJoueurs = new List<Label>();
            this.lblPetJoueurs = new List<Label>();
            this.lblArgentJoueurs = new List<Label>();
            this.lblBonsJoueurs = new List<Label>();
            this.lblIndJoueurs = new List<Label>();
            this.lblGrainJoueurs = new List<Label>();
            this.tblScores.Controls.Add(new Label(), 0, 0);
            i = 1;
            foreach (Joueur joueur in listeJoueurs)
            {
                lab = new Label();
               
                lab.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
                lab.AutoSize = true;
                lab.Location = new System.Drawing.Point(position, 1);
                lab.Margin = new System.Windows.Forms.Padding(0);
                lab.Name = "lblNomJoueur" + (i+1).ToString();
                lab.Size = new System.Drawing.Size(84, 18);
                lab.Text = joueur.NomJoueur;
                lab.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
                this.lblNomsJoueurs.Add(lab);
                position += 86;
                ++i;
            }
            i = 1;
            position = 0;
            foreach (Joueur joueur in listeJoueurs)
            {
                lab = new Label();
                lab.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                       | System.Windows.Forms.AnchorStyles.Left)
                       | System.Windows.Forms.AnchorStyles.Right)));
                lab.AutoSize = true;
                lab.BackColor = System.Drawing.SystemColors.Control;
                lab.Location = new System.Drawing.Point(position, 20);
                lab.Margin = new System.Windows.Forms.Padding(0);
                lab.Name = "lblMontantJoueur" + (i + 1).ToString(); 
                lab.Size = new System.Drawing.Size(84, 18);
                lab.Text = joueur.Cash.ToString();
                lab.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
                this.lblMontantJoueurs.Add(lab);
                position += 86;
                ++i;
            }
            i = 1;
            position = 0;
            foreach (Joueur joueur in listeJoueurs)
            {
                lab = new Label();
                lab.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
                lab.AutoSize = true;
                lab.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
                lab.Location = new System.Drawing.Point(position, 39);
                lab.Margin = new System.Windows.Forms.Padding(0);
                lab.Name = "lblOrJoueur" + (i + 1).ToString(); 
                lab.Size = new System.Drawing.Size(84, 18);
                lab.Text = joueur.Actions[Convert.ToInt32(IDActions.OR)].QteAction.ToString();
                lab.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
                this.lblOrJoueurs.Add(lab);
                position += 86;
                ++i; 
            }
            i = 1;
            position = 0;
            foreach (Joueur joueur in listeJoueurs)
            {
                lab = new Label();
                lab.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
                lab.AutoSize = true;
                lab.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
                lab.Location = new System.Drawing.Point(position, 58);
                lab.Margin = new System.Windows.Forms.Padding(0);
                lab.Name = "lblPetJoueur" + (i + 1).ToString();
                lab.Size = new System.Drawing.Size(84, 18);
                lab.Text = joueur.Actions[Convert.ToInt32(IDActions.PETROLE)].QteAction.ToString();
                lab.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
                this.lblPetJoueurs.Add(lab);
                position += 86;
                ++i;
            }
            i = 1;
            position = 0;
            foreach(Joueur joueur in listeJoueurs)
            {
                lab = new Label();
                lab.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
                lab.AutoSize = true;
                lab.BackColor = System.Drawing.Color.Silver;
                lab.Location = new System.Drawing.Point(position, 77);
                lab.Margin = new System.Windows.Forms.Padding(0);
                lab.Name = "lblArgentJoueur" + (i + 1).ToString();
                lab.Size = new System.Drawing.Size(84, 18);
                lab.Text = joueur.Actions[Convert.ToInt32(IDActions.ARGENT)].QteAction.ToString();
                lab.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
                this.lblArgentJoueurs.Add(lab);
                position += 86;
                ++i;
            }
            i = 1;
            position = 0;
            foreach(Joueur joueur in listeJoueurs)
            {
                lab = new Label();
                lab.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                       | System.Windows.Forms.AnchorStyles.Left)
                       | System.Windows.Forms.AnchorStyles.Right)));
                lab.AutoSize = true;
                lab.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
                lab.Location = new System.Drawing.Point(position, 96);
                lab.Margin = new System.Windows.Forms.Padding(0);
                lab.Name = "lblBonsJoueur" + (i + 1).ToString(); 
                lab.Size = new System.Drawing.Size(84, 20);
                lab.Text = joueur.Actions[Convert.ToInt32(IDActions.BONS)].QteAction.ToString();
                lab.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
                this.lblBonsJoueurs.Add(lab);
                position += 86;
                ++i;
            }
            i = 1;
            position = 0;
            foreach(Joueur joueur in listeJoueurs)
            {
                lab = new Label();
                lab.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                       | System.Windows.Forms.AnchorStyles.Left)
                       | System.Windows.Forms.AnchorStyles.Right)));
                lab.AutoSize = true;
                lab.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
                lab.Location = new System.Drawing.Point(position, 117);
                lab.Margin = new System.Windows.Forms.Padding(0);
                lab.Name = "lblIndJoueur" + (i + 1).ToString();
                lab.Size = new System.Drawing.Size(84, 20);
                lab.TabIndex = 14;
                lab.Text = joueur.Actions[Convert.ToInt32(IDActions.INDUSTRIEL)].QteAction.ToString(); 
                lab.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
                this.lblIndJoueurs.Add(lab);
                position += 86;
                ++i;
            }
            i = 1;
            position = 0;
            foreach (Joueur joueur in listeJoueurs)
            {
                lab = new Label();
                lab.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
                lab.AutoSize = true;
                lab.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
                lab.Location = new System.Drawing.Point(position, 138);
                lab.Margin = new System.Windows.Forms.Padding(0);
                lab.Name = "lblGrainJoueur" + (i + 1).ToString();
                lab.Size = new System.Drawing.Size(84, 23);
                lab.Text = joueur.Actions[Convert.ToInt32(IDActions.GRAIN)].QteAction.ToString(); 
                lab.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
                this.lblGrainJoueurs.Add(lab);
                position += 86;
                ++i;
            }

            i = 1;           
            foreach (Label lbl in this.lblNomsJoueurs)
            {
                this.tblScores.Controls.Add(lbl,i,0);
                ++i;
            }

            i = 1;
            this.tblScores.Controls.Add(this.lblMontant,0,1);
            foreach (Label lbl in this.lblMontantJoueurs)
            {
                this.tblScores.Controls.Add(lbl);
                ++i;
            }
            i = 1;
            this.tblScores.Controls.Add(this.lblQteOr, 0, 2);
            foreach (Label lbl in this.lblOrJoueurs)
            {
                this.tblScores.Controls.Add(lbl);
                ++i;
            }
            i = 1;
            this.tblScores.Controls.Add(this.lblQteArgent, 0, 3);
            foreach (Label lbl in this.lblArgentJoueurs)
            {
                this.tblScores.Controls.Add(lbl);
                ++i;
            }
            i = 1;
            this.tblScores.Controls.Add(this.lblQtePetrole, 0, 4);
            foreach (Label lbl in this.lblPetJoueurs)
            {
                this.tblScores.Controls.Add(lbl);
                ++i;
            }
            
            i = 1;
            this.tblScores.Controls.Add(this.lblQteBons, 0, 5);
            foreach (Label lbl in this.lblBonsJoueurs)
            {
                this.tblScores.Controls.Add(lbl);
                ++i;
            }
            i = 1;
            this.tblScores.Controls.Add(this.lblQteIndustrielles, 0, 6);
            foreach (Label lbl in this.lblIndJoueurs)
            {
                this.tblScores.Controls.Add(lbl);
                ++i;
            }
            i = 1;
            this.tblScores.Controls.Add(this.lblQteGrain, 0, 7);
            foreach (Label lbl in this.lblGrainJoueurs)
            {
                this.tblScores.Controls.Add(lbl);
                ++i;
            }
        }
        
        /// <summary>
        /// Fonction qui initialise la table des scores.
        /// </summary>
        private void InitialiseTableScores()
        {
            this.tblScores.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tblScores.ColumnCount = _nbreJoueurs+1;
            this.tblScores.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            for (int i = 0; i < _nbreJoueurs; ++i )
            {
                this.tblScores.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            }
            this.tblScores.Location = new System.Drawing.Point(6, 19);
            this.tblScores.Name = "tblScores";
            this.tblScores.RowCount = 8;
            this.tblScores.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tblScores.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tblScores.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tblScores.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tblScores.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tblScores.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tblScores.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tblScores.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tblScores.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tblScores.Size = new System.Drawing.Size(430, 162);
        }

        /// <summary>
        /// Fonction qui met à jour le tableau de scores des joueurs avec les dernières infos des joueurs.
        /// </summary>
        /// <param name="listeJoueurs"></param>
        public void UpdateTableScores(List<Joueur> listeJoueurs)
        {
            int i = 0;
            foreach (Joueur joueur in listeJoueurs)
            {                
                this.tblScores.GetControlFromPosition(joueur.NoJoueur, 1).Text = Convert.ToString(joueur.Cash);
                this.lblOrJoueurs[i].Text = Convert.ToString(joueur.Actions[0].QteAction);
                this.lblPetJoueurs[i].Text = Convert.ToString(joueur.Actions[1].QteAction);
                this.lblArgentJoueurs[i].Text = Convert.ToString(joueur.Actions[2].QteAction);
                this.lblBonsJoueurs[i].Text = Convert.ToString(joueur.Actions[3].QteAction);
                this.lblIndJoueurs[i].Text = Convert.ToString(joueur.Actions[4].QteAction);
                this.lblGrainJoueurs[i].Text = Convert.ToString(joueur.Actions[5].QteAction);
                ++i;
            }
            
        }
        /// <summary>
        /// Fonction qui met à jour la table des actions avec la dernière valeur des actions ainsi que leur fluctuation.
        /// </summary>
        /// <param name="valActions"></param>
        public void UpdateTableActions(Action[] valActions)
        {
            for (int i = 0; i < valActions.Length; ++i)
            {
                this.tblFluctuationsAct.GetControlFromPosition(0, i).Text = Convert.ToString(valActions[i].Position);
                this.tblFluctuationsAct.GetControlFromPosition(2, i).Text = Convert.ToString(Convert.ToDouble(valActions[i].ValeurAction)/100 + "$");
                this.tblFluctuationsAct.GetControlFromPosition(3, i).Text = valActions[i].DerniereFluctuation;
            }
        }
        /// <summary>
        /// Fonction qui met à jour le nombre de tours accomplit.
        /// </summary>
        /// <param name="toursFait"></param>
        /// <param name="toursTotal"></param>
        public void UpdateTours(int toursFait, int toursTotal)
        {
            tlsToursComplet.Text = Convert.ToString(toursFait) + "/" + Convert.ToString(toursTotal);
        }
        #endregion

        private System.Windows.Forms.GroupBox gbJoueurs;
        private System.Windows.Forms.GroupBox gbResultDes;
        private System.Windows.Forms.Button btnVendre;
        private System.Windows.Forms.Button btnAcheter;
        private System.Windows.Forms.Button btnRoulerDes;
        private System.Windows.Forms.MenuStrip mnuJeu;
        private System.Windows.Forms.ToolStripMenuItem jeuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nouvellePartieToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quitterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem paramsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aideToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem àProposToolStripMenuItem;
        private System.Windows.Forms.GroupBox gbActions;
        private System.Windows.Forms.Label lblActionGrains;
        private System.Windows.Forms.Label lblActionIndustrielles;
        private System.Windows.Forms.Label lblActionBons;
        private System.Windows.Forms.Label lblActionPetrole;
        private System.Windows.Forms.Label lblActionArgent;
        private System.Windows.Forms.Label lblActionOr;
        private System.Windows.Forms.NumericUpDown numOr;
        private System.Windows.Forms.NumericUpDown numArgent;
        private System.Windows.Forms.NumericUpDown numPetrole;
        private System.Windows.Forms.NumericUpDown numBons;
        private System.Windows.Forms.NumericUpDown numGrains;
        private System.Windows.Forms.NumericUpDown numIndustrielles;
        private System.Windows.Forms.TableLayoutPanel tblFluctuationsAct;
        private System.Windows.Forms.Label lblOr;
        private System.Windows.Forms.Label lblFlucGrains;
        private System.Windows.Forms.Label lblValGrains;
        private System.Windows.Forms.Label lblGrains;
        private System.Windows.Forms.Label lblFlucIndustrielles;
        private System.Windows.Forms.Label lblValIndustrielles;
        private System.Windows.Forms.Label lblIndustrielles;
        private System.Windows.Forms.Label lblFlucBons;
        private System.Windows.Forms.Label lblValBons;
        private System.Windows.Forms.Label lblBons;
        private System.Windows.Forms.Label lblFlucPetrole;
        private System.Windows.Forms.Label lblValPetrole;
        private System.Windows.Forms.Label lblPetrole;
        private System.Windows.Forms.Label lblFlucArgent;
        private System.Windows.Forms.Label lblValArgent;
        private System.Windows.Forms.Label lblArgent;
        private System.Windows.Forms.Label lblFlucOr;
        private System.Windows.Forms.Label lblValOr;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tlsTours;
        private System.Windows.Forms.TableLayoutPanel tblScores;
        private List<Label> lblNomsJoueurs;
        private List<Label> lblMontantJoueurs;
        private List<Label> lblOrJoueurs;
        private List<Label> lblPetJoueurs;
        private List<Label> lblArgentJoueurs;
        private List<Label> lblBonsJoueurs;
        private List<Label> lblIndJoueurs;
        private List<Label> lblGrainJoueurs;
        private System.Windows.Forms.Label lblQteIndustrielles;
        private System.Windows.Forms.Label lblQteBons;
        private System.Windows.Forms.Label lblQtePetrole;
        private System.Windows.Forms.Label lblQteArgent;
        private System.Windows.Forms.Label lblQteOr;      
        private System.Windows.Forms.Label lblQteGrain;
        private System.Windows.Forms.Label lblMontant;
        private System.Windows.Forms.Label lblPosGrain;
        private System.Windows.Forms.Label lblPosIndustrielles;
        private System.Windows.Forms.Label lblPosBons;
        private System.Windows.Forms.Label lblPosPetrole;
        private System.Windows.Forms.Label lblPosArgent;
        private System.Windows.Forms.Label lblPosOr;
        private System.Windows.Forms.GroupBox gbHistorique;
        private System.Windows.Forms.TextBox txtHistorique;
        public System.Windows.Forms.Label lblResultsDes;
        private System.Windows.Forms.ToolStripStatusLabel tlsToursComplet;
    }
}