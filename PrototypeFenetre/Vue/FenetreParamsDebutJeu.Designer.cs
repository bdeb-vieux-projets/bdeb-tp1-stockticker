﻿namespace StockTicker.Vue
{
    partial class FenetreParamsDebutJeu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbNbreTours = new System.Windows.Forms.GroupBox();
            this.optSoixanteTours = new System.Windows.Forms.RadioButton();
            this.optTrenteTours = new System.Windows.Forms.RadioButton();
            this.optDixTours = new System.Windows.Forms.RadioButton();
            this.gbNbreJoueurs = new System.Windows.Forms.GroupBox();
            this.optTroisJoueurs = new System.Windows.Forms.RadioButton();
            this.optQuatreJoueurs = new System.Windows.Forms.RadioButton();
            this.optDeuxJoueurs = new System.Windows.Forms.RadioButton();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.gbNbreTours.SuspendLayout();
            this.gbNbreJoueurs.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbNbreTours
            // 
            this.gbNbreTours.Controls.Add(this.optSoixanteTours);
            this.gbNbreTours.Controls.Add(this.optTrenteTours);
            this.gbNbreTours.Controls.Add(this.optDixTours);
            this.gbNbreTours.Location = new System.Drawing.Point(13, 13);
            this.gbNbreTours.Name = "gbNbreTours";
            this.gbNbreTours.Size = new System.Drawing.Size(207, 46);
            this.gbNbreTours.TabIndex = 0;
            this.gbNbreTours.TabStop = false;
            this.gbNbreTours.Text = "Nombre de tours:";
            // 
            // optSoixanteTours
            // 
            this.optSoixanteTours.AutoSize = true;
            this.optSoixanteTours.Location = new System.Drawing.Point(164, 20);
            this.optSoixanteTours.Name = "optSoixanteTours";
            this.optSoixanteTours.Size = new System.Drawing.Size(37, 17);
            this.optSoixanteTours.TabIndex = 2;
            this.optSoixanteTours.TabStop = true;
            this.optSoixanteTours.Text = "60";
            this.optSoixanteTours.UseVisualStyleBackColor = true;
            this.optSoixanteTours.CheckedChanged += new System.EventHandler(this.optSoixanteTours_CheckedChanged);
            // 
            // optTrenteTours
            // 
            this.optTrenteTours.AutoSize = true;
            this.optTrenteTours.Location = new System.Drawing.Point(85, 20);
            this.optTrenteTours.Name = "optTrenteTours";
            this.optTrenteTours.Size = new System.Drawing.Size(37, 17);
            this.optTrenteTours.TabIndex = 1;
            this.optTrenteTours.TabStop = true;
            this.optTrenteTours.Text = "30";
            this.optTrenteTours.UseVisualStyleBackColor = true;
            this.optTrenteTours.CheckedChanged += new System.EventHandler(this.optTrenteTours_CheckedChanged);
            // 
            // optDixTours
            // 
            this.optDixTours.AutoSize = true;
            this.optDixTours.Location = new System.Drawing.Point(6, 20);
            this.optDixTours.Name = "optDixTours";
            this.optDixTours.Size = new System.Drawing.Size(37, 17);
            this.optDixTours.TabIndex = 0;
            this.optDixTours.TabStop = true;
            this.optDixTours.Text = "10";
            this.optDixTours.UseVisualStyleBackColor = true;
            this.optDixTours.CheckedChanged += new System.EventHandler(this.optDixTours_CheckedChanged);
            // 
            // gbNbreJoueurs
            // 
            this.gbNbreJoueurs.Controls.Add(this.optTroisJoueurs);
            this.gbNbreJoueurs.Controls.Add(this.optQuatreJoueurs);
            this.gbNbreJoueurs.Controls.Add(this.optDeuxJoueurs);
            this.gbNbreJoueurs.Location = new System.Drawing.Point(13, 65);
            this.gbNbreJoueurs.Name = "gbNbreJoueurs";
            this.gbNbreJoueurs.Size = new System.Drawing.Size(207, 46);
            this.gbNbreJoueurs.TabIndex = 1;
            this.gbNbreJoueurs.TabStop = false;
            this.gbNbreJoueurs.Text = "Nombre de joueurs:";
            // 
            // optTroisJoueurs
            // 
            this.optTroisJoueurs.AutoSize = true;
            this.optTroisJoueurs.Location = new System.Drawing.Point(85, 20);
            this.optTroisJoueurs.Name = "optTroisJoueurs";
            this.optTroisJoueurs.Size = new System.Drawing.Size(31, 17);
            this.optTroisJoueurs.TabIndex = 2;
            this.optTroisJoueurs.TabStop = true;
            this.optTroisJoueurs.Text = "3";
            this.optTroisJoueurs.UseVisualStyleBackColor = true;
            this.optTroisJoueurs.CheckedChanged += new System.EventHandler(this.optTroisJoueurs_CheckedChanged);
            // 
            // optQuatreJoueurs
            // 
            this.optQuatreJoueurs.AutoSize = true;
            this.optQuatreJoueurs.Location = new System.Drawing.Point(164, 20);
            this.optQuatreJoueurs.Name = "optQuatreJoueurs";
            this.optQuatreJoueurs.Size = new System.Drawing.Size(31, 17);
            this.optQuatreJoueurs.TabIndex = 1;
            this.optQuatreJoueurs.TabStop = true;
            this.optQuatreJoueurs.Text = "4";
            this.optQuatreJoueurs.UseVisualStyleBackColor = true;
            this.optQuatreJoueurs.CheckedChanged += new System.EventHandler(this.optQuatreJoueurs_CheckedChanged);
            // 
            // optDeuxJoueurs
            // 
            this.optDeuxJoueurs.AutoSize = true;
            this.optDeuxJoueurs.Location = new System.Drawing.Point(6, 20);
            this.optDeuxJoueurs.Name = "optDeuxJoueurs";
            this.optDeuxJoueurs.Size = new System.Drawing.Size(31, 17);
            this.optDeuxJoueurs.TabIndex = 0;
            this.optDeuxJoueurs.TabStop = true;
            this.optDeuxJoueurs.Text = "2";
            this.optDeuxJoueurs.UseVisualStyleBackColor = true;
            this.optDeuxJoueurs.CheckedChanged += new System.EventHandler(this.optDeuxJoueurs_CheckedChanged);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(71, 117);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(69, 29);
            this.btnOk.TabIndex = 2;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(151, 117);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(69, 29);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Annuler";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // FenetreParamsDebutJeu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(232, 156);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.gbNbreJoueurs);
            this.Controls.Add(this.gbNbreTours);
            this.Name = "FenetreParamsDebutJeu";
            this.Text = "Paramètres du jeu";
            this.gbNbreTours.ResumeLayout(false);
            this.gbNbreTours.PerformLayout();
            this.gbNbreJoueurs.ResumeLayout(false);
            this.gbNbreJoueurs.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbNbreTours;
        private System.Windows.Forms.GroupBox gbNbreJoueurs;
        private System.Windows.Forms.RadioButton optSoixanteTours;
        private System.Windows.Forms.RadioButton optTrenteTours;
        private System.Windows.Forms.RadioButton optDixTours;
        private System.Windows.Forms.RadioButton optTroisJoueurs;
        private System.Windows.Forms.RadioButton optQuatreJoueurs;
        private System.Windows.Forms.RadioButton optDeuxJoueurs;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
    }
}