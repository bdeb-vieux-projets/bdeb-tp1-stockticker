﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StockTicker;

namespace StockTicker
{
    public enum IDJoueur { HUMAIN, AI };
    /// <summary>
    /// Classe qui va contenir les infos de chaque joueur dont le nom, l'argent, les actions qu'il possède.
    /// </summary>
    public class Joueur
    {               
        public Joueur(IDJoueur id, String nomJoueur, int numeroJoueur, int montantDepart)
        {
            this._idJoueur = id;
            this._noJoueur = numeroJoueur;
            this._cash = montantDepart;
            this._nomJoueur = nomJoueur;
            InitialiserActionsJoueur();
        }

        private String _nomJoueur;
        public String NomJoueur
        {
            get { return _nomJoueur; }
            set { _nomJoueur = value; }
        }

        private IDJoueur _idJoueur;
        public IDJoueur IdJoueur
        {
            get{ return _idJoueur; }
            set{ _idJoueur = value; }
        }

        private int _noJoueur;
        public int NoJoueur
        {
            get { return _noJoueur; }
            set{ _noJoueur = value; }
        }

        private int _cash;
        public int Cash
        {
            get { return _cash; }
            set{ _cash = value; }
        }

        private List <Action> _actions;
        public List <Action> Actions
        {
            get {return _actions;}
            set { _actions = value; }
        }

       // public void (VendreActions)

        private void InitialiserActionsJoueur()
        {
            this.Actions = new List<Action>();
            Action uneAction = new Action(IDActions.OR, 0);
            this.Actions.Add(uneAction);
            uneAction = new Action(IDActions.ARGENT, 0);
            this.Actions.Add(uneAction);
            uneAction = new Action(IDActions.BONS, 0);
            this.Actions.Add(uneAction);
            uneAction = new Action(IDActions.PETROLE, 0);
            this.Actions.Add(uneAction);
            uneAction = new Action(IDActions.INDUSTRIEL, 0);
            this.Actions.Add(uneAction);
            uneAction = new Action(IDActions.GRAIN, 0);
            this.Actions.Add(uneAction);

            foreach (Action act in this.Actions)
            {
                act.QteAction = 0;
            }
        }        
    }
}
