﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StockTicker.Vue;
namespace StockTicker
{
    public partial class MenuPrincipal : Form
    {
        public MenuPrincipal()
        {
            InitializeComponent();
        }

        private void MenuPrincipal_Load(object sender, EventArgs e)
        {

        }

        private void btnCommencer_Click(object sender, EventArgs e)
        {
            
            this.Hide();
            FenetreParamsDebutJeu optionsDebut = new FenetreParamsDebutJeu();
            optionsDebut.ShowDialog();
            //FenetreJeu jeu = new FenetreJeu();
           
           // jeu.ShowDialog();
            this.Close();
        }

        private void btnQuitter_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
