﻿namespace StockTicker
{
    partial class FenetreOption
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dlgColorAction = new System.Windows.Forms.ColorDialog();
            this.btnAnnuler = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.lstCouleursActions = new System.Windows.Forms.ListBox();
            this.lblCouleur = new System.Windows.Forms.Label();
            this.numTours = new System.Windows.Forms.NumericUpDown();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblNbTours = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numTours)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnAnnuler
            // 
            this.btnAnnuler.Location = new System.Drawing.Point(185, 191);
            this.btnAnnuler.Name = "btnAnnuler";
            this.btnAnnuler.Size = new System.Drawing.Size(75, 23);
            this.btnAnnuler.TabIndex = 4;
            this.btnAnnuler.Text = "Annuler";
            this.btnAnnuler.UseVisualStyleBackColor = true;
            this.btnAnnuler.Click += new System.EventHandler(this.btnAnnuler_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(104, 191);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 5;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // lstCouleursActions
            // 
            this.lstCouleursActions.FormattingEnabled = true;
            this.lstCouleursActions.Items.AddRange(new object[] {
            "Or ",
            "Argent",
            "Pétrole",
            "Industriel",
            "Bonds",
            "Grain"});
            this.lstCouleursActions.Location = new System.Drawing.Point(9, 19);
            this.lstCouleursActions.Name = "lstCouleursActions";
            this.lstCouleursActions.Size = new System.Drawing.Size(100, 95);
            this.lstCouleursActions.TabIndex = 6;
            // 
            // lblCouleur
            // 
            this.lblCouleur.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lblCouleur.Location = new System.Drawing.Point(118, 19);
            this.lblCouleur.Name = "lblCouleur";
            this.lblCouleur.Size = new System.Drawing.Size(32, 23);
            this.lblCouleur.TabIndex = 7;
            // 
            // numTours
            // 
            this.numTours.Location = new System.Drawing.Point(122, 152);
            this.numTours.Name = "numTours";
            this.numTours.Size = new System.Drawing.Size(40, 20);
            this.numTours.TabIndex = 10;
            this.numTours.ValueChanged += new System.EventHandler(this.numTours_ValueChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(173, 19);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 11;
            this.button1.Text = "Changer";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.lstCouleursActions);
            this.groupBox1.Controls.Add(this.lblCouleur);
            this.groupBox1.Location = new System.Drawing.Point(12, 11);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(254, 125);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Couleur des actions:";
            // 
            // lblNbTours
            // 
            this.lblNbTours.AutoSize = true;
            this.lblNbTours.Location = new System.Drawing.Point(18, 154);
            this.lblNbTours.Name = "lblNbTours";
            this.lblNbTours.Size = new System.Drawing.Size(88, 13);
            this.lblNbTours.TabIndex = 9;
            this.lblNbTours.Text = "Nombre de tours:";
            this.lblNbTours.Click += new System.EventHandler(this.lblNbTours_Click);
            // 
            // FenetreOption
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(278, 223);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.numTours);
            this.Controls.Add(this.lblNbTours);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnAnnuler);
            this.Name = "FenetreOption";
            this.Text = "Parametres jeu";
            ((System.ComponentModel.ISupportInitialize)(this.numTours)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ColorDialog dlgColorAction;
        private System.Windows.Forms.Button btnAnnuler;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.ListBox lstCouleursActions;
        private System.Windows.Forms.Label lblCouleur;
        private System.Windows.Forms.NumericUpDown numTours;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblNbTours;
    }
}