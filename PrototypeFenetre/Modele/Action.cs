﻿using System.Drawing;
using System;
//using StockTicker.Modele;

namespace StockTicker
{
    public enum IDActions { OR, ARGENT, PETROLE, BONS, INDUSTRIEL, GRAIN };
    public class Action
    {
       /// <summary>
       /// Classe qui va contenir les infos sur une action ainsi que sa valeur, la quantité d'action et son id de type.
       /// </summary>
       /// <param name="idAction"></param>
       /// <param name="qteAction"></param>
        public Action(IDActions idAction, int qteAction)
        {
            this._idAction = idAction;
            this._qteAction = qteAction;
        }

        public Action(IDActions idAction, int valeur, int position)
        {
            this._idAction = idAction;
            this._valeurAction = valeur;
            this._position = position;
        }
        private Color _couleurAction;
        public Color CouleurAction
        {
            get{ return _couleurAction; }
            set{ _couleurAction = value; }
        }

        private IDActions _idAction;
        public IDActions IdAction
        {
            get { return _idAction; }
            set { _idAction = value; }
        }

        private int _qteAction;
        public int QteAction
        {
            get { return _qteAction; }
            set { _qteAction = value; }
        }
        private int _position;
        public int Position
        {
            get { return _position; }
            set { _position = value; }
        }
        private int _valeurAction;
        public int ValeurAction
        {
            get { return _valeurAction; }
            set { _valeurAction = value; }
        }
        private String _derniereFluctuation;
        public String DerniereFluctuation
        {
            get { return _derniereFluctuation; }
            set { _derniereFluctuation = value; }
        }       
    }
}
