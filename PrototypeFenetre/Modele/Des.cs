﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StockTicker
{
  /// <summary>
  /// Classe qui contient les informations sur les dés.
  /// </summary>
    public class Des
    {
        public readonly String[] FACES_ACTIONS =
            {"Or","Argent","Pétrole","Industriel","Bonds","Grain"};
        public readonly String[] FACES_FLUCTUATION = 
            {"monte de","descends de","paye"};
        public readonly int[] FACES_VALEURS = 
            {5,10,20};

        private IDActions _deAction;
        public IDActions DeAction
        {
            get
            {
                return _deAction;
            }
            set
            {
                _deAction = value;
            }
        }

        private String _deFluctuation;
        public String DeFluctuation
        {
            get
            {
                return _deFluctuation;
            }
            set { _deFluctuation = value; }
        }

        private int _deValeur;
        public int DeValeur
        {
            get
            {
                return _deValeur;
            }
            set
            {
                _deValeur = value;
            }
        }
        /// <summary>
        /// Fonction qui se charge de rouler les dés.
        /// </summary>
        public void RoulerDes()
        {
            Random rnd = new Random();
            int faceAction = rnd.Next(0,6);
            this._deAction = (IDActions)rnd.Next(0, 5);
            int faceFluctuation = rnd.Next(0, 3);
            this._deFluctuation = FACES_FLUCTUATION[faceFluctuation];
            int faceValeur = rnd.Next(0,2);
            this._deValeur = FACES_VALEURS[faceValeur];
            Console.WriteLine(_deAction + " " + _deFluctuation + " " + _deValeur);
            faceAction = faceFluctuation = faceValeur = 50;
        }
    }
    
}
